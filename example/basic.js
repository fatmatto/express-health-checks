const express = require('express')
const app = express()
const health = require('../index')
const mongodb = require('mongodb').MongoClient
const mysql = require('mysql')

mongodb.connect('mongodb://localhost:27017/test', (err, client) => {
  if (err) { throw err }
  console.log('connected to mongodb')
  let MySqlConnection = mysql.createConnection('mysql://root:root@localhost:3306/Logs')
  let db = client.db('test')
  MySqlConnection.on('error', (error) => {
    console.log("MysqlError",error)
  })
  MySqlConnection.connect((err) => {
    if (err) { throw err }
    console.log('Conencted to mysql')
    app.use('/health', health({
      http: { enabled: true },
      mongodb: { enabled: true, client: () => { return db } },
      mysql: { enabled: true, client: MySqlConnection }
    }))

    app.listen(3000, () => {
      console.log('Listening on port 3000')
    })
  })
})
