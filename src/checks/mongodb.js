module.exports = (configuration, callback) => {
  let client = configuration.mongodb.client
  if (typeof client === 'function') {
    client = client()
  }
  client.stats((err, results) => {
    if (err) {
      callback(false) // eslint-disable-line standard/no-callback-literal
    } else {
      callback(true) // eslint-disable-line standard/no-callback-literal
    }
  })
}
