module.exports = (configuration, callback) => {
  let client = configuration.mysql.client
  if (typeof client === 'function') {
    client = client()
  }
  client.query('SELECT 1;', (err, results) => {
    if (err) {
      callback(false) // eslint-disable-line standard/no-callback-literal
    } else {
      callback(true) // eslint-disable-line standard/no-callback-literal
    }
  })
}
