## Express health checks

This package includes a middleware that helps you create healthchecks for you expressjs application

*It is very early stage, not even close to a complete health check framework for your production app :)*

### Basic example

```javascript
const express = require('express')
const app = express()
const health = require('../index')
const mongodb = require('mongodb').MongoClient
const mysql = require('mysql')

mongodb.connect('mongodb://localhost:27017/test', (err, client) => {
  if (err) { throw err }

  // We will use this reference to make sure that the app is connected to the database.
  // Note that this is very unrealistic, you should have a connection pool and this test should make sure
  // that your express app is able to pull a connection from the pool and make a basic query.
  let mongoClient = client.db('test')

  // Same important notes about mongodb applies here.
  let MySqlConnection = mysql.createConnection('mysql://@localhost:3306/test')

  
  MySqlConnection.connect((err) => {
    if (err) { throw err }

    // Here we use add the 
    app.use('/health', health({
      http: { enabled: true },
      mongodb: { enabled: true, client: mongoClient },
      mysql: { enabled: true, client: MySqlConnection }
    }))

    app.listen(3000, () => {
      console.log('Listening on port 3000')
    })
  })
})

```