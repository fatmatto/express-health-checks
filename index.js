const express = require('express')

/**
 *
 * @example
 * app.use(expressHealthChecks({
 *   http : { enabled: true},
 *   mongodb : { enabled: true, connection : mongoClient },
 *   mysql : { enabled: true, connection: mysqlClient }
 * }))
 */
module.exports = function (config) {
  config = config || {}
  let router = express.Router()

  router.get('/http', (req, res, next) => {
    return res.sendStatus(200)
  })

  if (config.mongodb && config.mongodb.enabled) {
    let mongodbHealthChecks = require('./src/checks/mongodb')
    router.get('/mongodb', (req, res, next) => {
      mongodbHealthChecks(config, (outcome) => {
        if (outcome) {
          return res.sendStatus(200)
        } else {
          return res.sendStatus(400)
        }
      })
    })
  }

  if (config.mysql && config.mysql.enabled) {
    let mysqlHealthChecks = require('./src/checks/mysql')
    router.get('/mysql', (req, res, next) => {
      mysqlHealthChecks(config, (outcome) => {
        if (outcome) {
          return res.sendStatus(200)
        } else {
          return res.sendStatus(400)
        }
      })
    })
  }

  return router
}
